import React from 'react';
import {Auth} from "./components/auth/auth";

function App() {
    return (
        <>
            <Auth/>
        </>
    );
}

export default App;
