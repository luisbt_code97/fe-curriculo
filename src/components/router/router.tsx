import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Home} from "../home/home";
import {Auth} from "../auth/auth";
import {LayoutComponent} from "../layout/layout";

/**
 * @component Router
 * @constructor
 */
export const RouterComponent = () => {
    return (
        <>
            <Router>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/home" component={Home}/>
                    <Route exact={true} path="/auth" component={Auth}/>
                    <Route exact path="/homeUser" component={LayoutComponent}/>
                </Switch>
            </Router>
        </>
    );
}

