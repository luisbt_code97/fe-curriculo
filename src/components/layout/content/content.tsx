import {Content} from "antd/es/layout/layout";
import {IPropsChildren} from "../../../commons/interfaces/Props/props";

/**
 * @Component ContentComponent
 * @param children
 * @constructor
 */
export const ContentComponent = ({children}: IPropsChildren) => {
    return (
        <Content style={{margin: '0 16px'}}>
            {children}
        </Content>
    )
}
