import {Header} from "antd/lib/layout/layout";

/**
 * @component HeaderComponent
 * @constructor
 */
export const HeaderComponent = () => {
    return (
        <Header className="site-layout-background" style={
            {background: 'white', height: 150, position: 'relative', top: '34px', width: '90%', margin: '1% auto'}
        }>
            {
                    <h1 className="title-header">Documentos</h1>
            }
        </Header>
    )
}
