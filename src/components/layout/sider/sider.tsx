import {Layout, Menu} from "antd";
import {Link} from 'react-router-dom';

const {Sider} = Layout;
const {Item} = Menu;
/**
 * @component SiderComponent
 * @constructor
 */
export const SiderComponent = () => {
    return (
        <Sider theme='light' collapsible>
            <div className="logo"/>
            {/** Menu **/}
            <Menu theme="light" mode="inline" defaultSelectedKeys={['1']}>
                <Item key="1">
                    <Link to="/HomeUser">Inicio</Link>
                </Item>
                <Item key="2">
                    <Link to="/CreateDocument">Crear Documento</Link>
                </Item>
            </Menu>
        </Sider>
    )
}
