import {Layout} from 'antd';
import {SiderComponent} from "./sider/sider";
import {HeaderComponent} from "./header/header";
import {ContentComponent} from "./content/content";

/**
 * @component Layout component
 * @constructor
 */
export const LayoutComponent = () => {
    return (
        <Layout style={{minHeight: '100vh'}}>
            {/** Sider **/}
            <SiderComponent/>
            <Layout className="site-layout">
                {/** Header **/}
                <HeaderComponent/>
                {/** Content **/}
                <ContentComponent>
                  <h1>Hello</h1>
                </ContentComponent>
            </Layout>
        </Layout>
    )
}
