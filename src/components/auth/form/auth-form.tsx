import {Button, Col, Form, Input, Row} from "antd";
import {FormattedMessage} from "react-intl";
import {ChangeButtonPrincipal, ChangeInput} from "../../../styles/framework/antd-framework";
import {FC} from "react";
import {IForms} from "../../../commons/interfaces/forms/forms";
import {useHistory} from "react-router-dom";

/**
 * Destructuring Form
 */
const {Item} = Form;
/**
 * Component AuthForm
 * @constructor
 */
export const AuthForm: FC<IForms> = (props) => {
    /**
     * Hook useHistory
     */
    const history = useHistory();
    return (
        <div className="forms">
            <Form form={props.form} onFinish={props.handlerClick} layout='vertical'>
                <Row gutter={24}>
                    {/** Email **/}
                    <Col className="gutter-row" span={24}>
                        <Item name="email" label="Email" rules={[
                            {
                                type: 'email',
                                message: 'Correo invalido'
                            },
                            {
                                required: true,
                                message: 'Correo requerido*'
                            }
                        ]}>
                            <Input style={{...ChangeInput}} type="email"/>
                        </Item>
                    </Col>
                    {/** Password **/}
                    <Col className="gutter-row" span={24}>
                        <Item name="password" label="Contraseña" rules={[
                            {
                                required: true,
                                message: 'Contraseña requerida*'
                            }
                        ]}>
                            <Input.Password style={{...ChangeInput}} type="password"/>
                        </Item>
                    </Col>
                    {/** Button **/}
                    <Col className="gutter-row" span={24}>
                        <Button htmlType="submit"
                                style={{...ChangeButtonPrincipal}}
                        >
                            <FormattedMessage id="Continue"/>
                        </Button>
                        <Button htmlType="submit"
                                onClick={() => history.push('/home')}
                                style={{...ChangeButtonPrincipal, background: 'white', color: 'black'}}
                        >
                            <FormattedMessage id="Auth.Home"/>
                        </Button>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}
