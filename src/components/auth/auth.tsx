import {FormattedMessage} from "react-intl";
import {FontGeneric} from "../generics-components/Fonts/font-generic";
import {AuthForm} from "./form/auth-form";
import {Form} from "antd";
import {useAuthHook} from "../../hooks/Auth/auth-hook";

/**
 * Component Auth
 * @constructor
 */
export const Auth = () => {
    const {login} = useAuthHook();
    /**
     * form hook
     */
    const [form] = Form.useForm();
    const handlerClick = async (value: any) => {
        await login(value);
    }
    return (
        <div className="auth-container">
            <FontGeneric textAlign="center" style={{margin: '2% auto'}} fontWeight="bold" fontSize="100px" color="Red"
                         content={<FormattedMessage id="Auth.Login"/>}/>
            <div className="container-auth-form">
                <AuthForm form={form} handlerClick={handlerClick}/>
            </div>
        </div>
    )
}
