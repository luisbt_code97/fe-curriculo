import {FontGeneric} from "../generics-components/Fonts/font-generic";
import {FormattedMessage} from "react-intl";
import {useHistory} from "react-router-dom";
import {Button} from "antd";
import {ChangeButtonPrincipal} from "../../styles/framework/antd-framework";

/**
 * Component home
 * @constructor
 */
export const Home = () => {
    /**
     * Hook history
     */
    const history = useHistory();
    const handleClick = () => {
        history.push('/auth');
    }
    return (
        <div className="home-container">
            <div className='home-content'>
                <FontGeneric textAlign="center" fontWeight="bold" fontSize="150px" color="Red"
                             content={<FormattedMessage id="Title.Control"/>}/>
                <FontGeneric style={{marginTop: '-5%'}}
                             textAlign="center"
                             fontWeight="bold"
                             fontSize="150px"
                             color="black"
                             content={<FormattedMessage id="Title.Curriculo"/>}
                />
                <FontGeneric textAlign="center" fontWeight="400" fontSize="30px" style={{width: '60%', margin: 'auto'}} color="black"
                             content={<FormattedMessage id="Content.ControlCurriculo"/>}
                />
            </div>
            {/** Button Login **/}
            <div className="home-button-login">
                <Button htmlType="button"
                        style={{...ChangeButtonPrincipal, width: '20%'}}
                        onClick={handleClick}
                >
                    {<FormattedMessage id="Auth.Login"/>}
                </Button>
            </div>
            <FontGeneric textAlign="center" fontWeight="100" fontSize="27px" style={{width: '60%', margin: '5% auto'}} color="black"
                         content={<FormattedMessage id="ExploreControlCurriculo"/>}
            />
        </div>
    )
}
