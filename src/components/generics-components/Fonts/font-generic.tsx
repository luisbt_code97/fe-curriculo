import {FC} from "react";
import {IFont} from "../../../commons/interfaces/generics-components/generic-components";

/**
 * @component Font
 * @param props
 * @constructor
 */
export const FontGeneric: FC<IFont> = (props) => {
    const {color, fontSize, content, fontWeight, textAlign, style} = props;
    return (
        <h1 style={{color, fontSize, fontWeight, textAlign, ...style}}>{content}</h1>
    )
}
