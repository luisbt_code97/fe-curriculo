import Axios from 'axios';
import {URL_HOST} from "../commons/const/enviroment";

/**
 * @class BaseSingleton
 */
export class BaseHTTPSingleton {

    private static basicSingleton: BaseHTTPSingleton;

    private constructor() {
    }

    public static GetInstance(): BaseHTTPSingleton {
        if (BaseHTTPSingleton.basicSingleton === undefined) {
            return new BaseHTTPSingleton();
        } else {
            return BaseHTTPSingleton.basicSingleton;
        }
    }

    async Post<T>(body: T, url: string): Promise<string | any> {
        try {
            const {data} = await Axios.post(URL_HOST + url, {
                ...body
            });
            return data;
        } catch (e) {
            return 'Error internal';
        }
    }

    async Get(url?: string): Promise<any> {
        try {
            const {data} = await Axios.get(URL_HOST + url);
            return data;
        } catch (e) {
            return 'Error internal';
        }
    }

}
