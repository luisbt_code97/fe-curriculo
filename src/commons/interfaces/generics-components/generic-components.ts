/**
 * @interface for buttons
 */
import {EInputs} from "../../enums/enums";

export interface IButtonGenericComponent {
    color: string;
    onClickFunction?: (color: string) => void;
    background: string;
    fontSize: string;
    content: any;
}

/**
 * @interface for Font
 */
export interface IFont {
    fontSize: string;
    color: string;
    content: any;
    fontWeight: string | any;
    textAlign: string | any;
    style?: any;
}

/**
 * @interface for Input
 */
export interface IInput {
    typeInput: EInputs,
    style?: any
}





