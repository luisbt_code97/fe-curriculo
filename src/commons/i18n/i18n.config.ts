import {II18N} from "../interfaces/i18n/i18";


export const I18nConfig: II18N = {
    locale: 'en',
    messages: {
        'Title.Control': 'Control',
        'Title.Curriculo': 'Curriculo',
        'Content.ControlCurriculo': 'Gestor de documentos institucionales, tales como acuerdo pedagogicos, seguimientos y plan de curso de la' +
            ' UNIAJC.',
        'Auth.Login': 'Log in',
        'Auth.Home': 'Volver al inicio',
        'Auth.SignIn': 'Registrarse',
        'ExploreControlCurriculo': 'Explora control curriculo',
        'Continue': 'Continuar',
    }
}
