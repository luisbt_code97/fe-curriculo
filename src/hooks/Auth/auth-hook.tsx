import {useCallback} from "react";
import {BaseHTTPSingleton} from "../../services/basehttp-singleton";
import {AuthDTO} from "../../commons/DTO/auth";
import {message} from "antd";
import { useHistory } from 'react-router-dom';
/**
 * Hook by Authentication
 * @component
 */
export const useAuthHook = () => {
    /**
     * Hook history
     */
    const history = useHistory();
    const login = useCallback(async (auth: AuthDTO) => {
        try {
            const response = await BaseHTTPSingleton.GetInstance().Post(auth, '/Auth/SignIn');
            if (response.message && response.message !== 'No data created') {
                localStorage.setItem('user', JSON.stringify(response.message));
                setTimeout(() => {
                    history.push('/home');
                }, 2000);
            } else {
                message.error('Usuario no encontrado');
            }
        } catch (e) {
            message.error('Error interno');
        }
    }, [history]);

    const logout = useCallback(() => {
        localStorage.removeItem('user');
    }, []);

    return {
        login,
        logout
    }
}
