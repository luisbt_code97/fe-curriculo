export const ChangeButtonPrincipal: any = {
    background: 'black',
    color: 'white',
    fontSize: '15px',
    width: '45%',
    height: '60px',
    display: 'block',
    margin: ' 5% auto',
    fontWeight: 'bold',
    borderRadius: '10px'
}

export const ChangeInput: any = {borderRadius: '15px', height: '45px'}
