import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import {IntlProvider} from "react-intl";
import {I18nConfig} from "./commons/i18n/i18n.config";
import "./styles/main.css";
import {RouterComponent} from "./components/router/router";


ReactDOM.render(
    <IntlProvider defaultLocale={I18nConfig.locale} locale={I18nConfig.locale} messages={I18nConfig.messages}>
        <RouterComponent/>
    </IntlProvider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
